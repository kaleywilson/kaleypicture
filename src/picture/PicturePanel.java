package picture;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

/**
 * A class draws a picture on a panel
 *
 */
@SuppressWarnings("serial")
public class PicturePanel extends JPanel {
	int currentPage;
	int numPages;

	// array to store lines drawn by user
	ArrayList<UserLine> userLines;
	// stores the current userLine being created by the user
	UserLine userLine;

	// HashMaps for each of the pages which stores, the name of the grouping and the
	// ColorShape instance
	// that holds the shapes and the general area that needs to be clicked by the
	// user
	HashMap<String, ColorShape> flag;
	HashMap<String, ColorShape> windmill;
	HashMap<String, ColorShape> hope;
	HashMap<String, ColorShape> welcome;

	// create buttonpanel
	ButtonPanel nextBack;

	// create colorpanel
	ColorPanel colorpanel;

	/**
	 * Get stuff ready so when paintComponent is called, it can draw stuff.
	 * Depending on how complicated you want to get, your constructor may be blank.
	 */
	public PicturePanel() {
		// set layout
		setLayout(new BorderLayout());

		// mousehandler
		MouseHandler mh = new MouseHandler();
		addMouseListener(mh);
		addMouseMotionListener(mh);

		// the current page that is associated with each of the 'pages' shown
		currentPage = 0;
		numPages = 3; // pages 0-3, so 4 pages

		// create individual pages
		flag = new HashMap<>();
		windmill = new HashMap<>();
		hope = new HashMap<>();
		welcome = new HashMap<>();

		// create groupings of ColorShapes to click on
		createGroupings();

		// initialize button in center of the bottom
		nextBack = new ButtonPanel();
		add(nextBack, BorderLayout.SOUTH);

		// initalize colorpanel on left side of page
		colorpanel = new ColorPanel();
		add(colorpanel, BorderLayout.NORTH);

		// userline creation
		userLines = new ArrayList<>();
		userLine = new UserLine(colorpanel.getColor(), colorpanel.getStroke());
	}

	// creates the different groupings on each page that can be selected to be
	// colored in
	public void createGroupings() {
		flag.put("Rect1", new ColorShape(345, 284, 75, 80));
		flag.put("Rect2", new ColorShape(600, 285, 65, 80));
		flag.put("Rect3", new ColorShape(350, 423, 65, 80));
		flag.put("Rect4", new ColorShape(603, 422, 65, 80));
		flag.put("Circle", new ColorShape(425, 325, 150, 150));

		windmill.put("Door", new ColorShape(465, 500, 70, 100));
		windmill.put("Windows", new ColorShape(410, 400, 180, 50));
		windmill.put("Base", new ColorShape(300, 230, 400, 370));
		windmill.put("Blade1", new ColorShape(260, 50, 500, 400));

		hope.put("H", new ColorShape(160, 255, 125, 145));
		hope.put("O", new ColorShape(330, 255, 140, 145));
		hope.put("P", new ColorShape(510, 255, 115, 145));
		hope.put("E", new ColorShape(660, 255, 115, 145));

	}

	// method draws the lines the user is drawing when paintComponent is called
	public void drawUserLines(Graphics g) {
		Graphics2D g1 = (Graphics2D) g;
		g1.setColor(userLine.getColor());
		g1.setStroke(userLine.getStroke());
		for (int i = userLine.size() - 1; i > 0; i--) {
			g1.draw(userLine.getLine(i));
		}
		for (UserLine line : userLines) {
			g1.setColor(line.getColor());
			g1.setStroke(line.getStroke());
			for (int i = line.size() - 1; i > 0; i--) {
				g1.draw(line.getLine(i));
			}
		}
	}

	// this method is used specifically for rectangles that need to be drawn 
	//it loops through the specific hashmap and the group of shapes and pushes them into
	//the method 'rotateRectangle' to be drawn
	public void drawShapes(Graphics g, HashMap<String, ColorShape> map) {
		for (String group : map.keySet()) {
			for (Shape shape : map.get(group).getShapes()) {
				rotateRectangle(g, (Rectangle) shape, group, map);
			}
		}
	}

	// Rotates and also draws and fills rectangles
	
	public void rotateRectangle(Graphics g, Rectangle r, String c, HashMap<String, ColorShape> map) {
		Graphics2D g1 = (Graphics2D) g;
		AffineTransform old = g1.getTransform();

		//uses a switch statement as some rectangles need to be rotates a specific amount
		switch (c) {
		case "Rect1":
		case "Rect4":
			g1.rotate(-45, r.getX(), r.getY());
			break;
		case "Rect2":
		case "Rect3":
			g1.rotate(-225, r.getX(), r.getY());
			break;
		case "Blade1":
		case "Blade2":
			g1.rotate(Math.toRadians(45), 500, 255);
			break;
		}

		//if color is on for the grouping, then it will the shapes with the color assigned
		//to the shape
		if (map.get(c).isColorOn()) {
			g1.setColor(map.get(c).getColor(r));
			g1.fill(r);
			g1.setColor(Color.black);
			g1.draw(r);

		} else {
			g1.setColor(Color.black);
			g1.draw(r);
		}
		
		//sets g1 to the old setting so not everything is rotated
		g1.setTransform(old);

	}

	// creates any rectangles and puts them in the HashMap in ColorShape
	//uses switch statements for the different hashmaps and for the different rectangles
	//to group them accordingly
	public void createRect(int dx, int dy, int p, String page) {
		Rectangle x;
		switch (page) {
		case "flag":
			for (int i = 3; i > 0; i--) {
				switch (p) {
				case 1:
					x = new Rectangle(dx, dy, 65, 8);
					flag.get("Rect1").addShape(x, Color.black);
					dx -= 14;
					dy -= 9;
					break;
				case 2:
					if (i != 2) {
						x = new Rectangle(dx, dy, 30, 8);
						flag.get("Rect2").addShape(x, Color.black);
						dy -= 33;
						dx -= 12;
						x = new Rectangle(dx, dy, 30, 8);
						flag.get("Rect2").addShape(x, Color.black);
					} else {
						x = new Rectangle(dx, dy, 65, 8);
						flag.get("Rect2").addShape(x, Color.black);
						dy += 33;
						dx += 12;
					}
					dx += 14;
					dy -= 6;
					break;
				case 3:
					if (i == 2) {
						x = new Rectangle(dx, dy, 30, 8);
						flag.get("Rect3").addShape(x, Color.black);
						dy -= 33;
						dx -= 12;
						x = new Rectangle(dx, dy, 30, 8);
						flag.get("Rect3").addShape(x, Color.black);
					} else {
						x = new Rectangle(dx, dy, 65, 8);
						flag.get("Rect3").addShape(x, Color.black);
						dy += 33;
						dx += 12;
					}
					dx += 14;
					dy -= 6;
					break;
				case 4:
					x = new Rectangle(dx, dy, 30, 8);
					flag.get("Rect4").addShape(x, Color.black);
					dy += 33;
					dx -= 18;

					x = new Rectangle(dx, dy, 30, 8);
					flag.get("Rect4").addShape(x, Color.black);
					dx += 30;
					dy -= 26;
				}
			}
			break;
		case "windmill":
			switch (p) {
			case 1:
				x = new Rectangle(dx, dy, 70, 100);
				windmill.get("Door").addShape(x, new Color(115, 67, 33));
				break;
			case 2:
				for (int i = 3; i > 0; i--) {
					x = new Rectangle(dx, dy, 50, 50);
					windmill.get("Windows").addShape(x, new Color(150, 90, 84));
					dx += 65;
				}
				break;
			case 3:
				x = new Rectangle(dx, dy, 600, 50);
				windmill.get("Blade1").addShape(x, new Color(171, 100, 50));

				dx = 475;
				dy = -45;
				x = new Rectangle(dx, dy, 50, 600);
				windmill.get("Blade1").addShape(x, new Color(171, 100, 50));
				break;
			}
		}

	}

	// creates welcome page
	public void createWelcome(Graphics g) {
		g.setFont(new Font("Bodoni MT", 25, 50));
		g.drawString("Welcome to \"Color My Life\"!", 175, 200);
		g.setFont(new Font("Corbel", 15, 20));
		g.drawString("On each page represents a picture that reflects something about me.", 175, 240);
		g.drawString("If you click on different parts of the picture, it will color it in to", 190, 270);
		g.drawString("give you more detail. Also you are free to add some of your own color by", 175, 300);
		g.drawString("selecting a color shown up top and the thickness.", 225, 325);
		g.drawString("If you want to go to the next page or go back, use the buttons below.", 175, 350);
		g.drawString("NOTE: After pushing the 'next' or 'back' button, push anywhere to go to the next page", 150, 375);
		g.setFont(new Font("Broadway", 175, 200));
		g.setColor(new Color(92, 224, 194));
		g.drawString("ENJOY!", 100, 600);
	}

	// creates the flag
	public void createFlag(Graphics g) {
		// draw outline
		g.drawRect(275, 250, 450, 300);

		// color in or draw outline of circle
		if (flag.get("Circle").isColorOn()) {
			Shape old = g.getClip();
			g.setClip(425, 325, 150, 75);
			g.setColor(Color.red);
			g.fillOval(425, 325, 150, 150);
			g.setColor(Color.blue);
			g.fillOval(495, 369, 78, 75);

			g.setClip(425, 400, 150, 75);
			g.fillOval(425, 325, 150, 150);
			g.setColor(Color.red);
			g.fillOval(425, 343, 75, 75);
			g.setClip(old);
			g.setColor(Color.black);

		} else {
			g.setColor(Color.black);
			g.drawOval(425, 325, 150, 150);
			g.drawArc(425, 343, 75, 75, 190, 160);
			g.drawArc(495, 369, 78, 75, 0, 160);
		}

		// draw the rectangles and color them if necessary
		createRect(375, 360, 1, "flag");
		createRect(620, 330, 2, "flag");
		createRect(360, 440, 3, "flag");
		createRect(620, 450, 4, "flag");

		// draw four quadrants surrounding circle
		drawShapes(g, flag);

		// put text
		g.setFont(new Font("SanSerif", 35, 30));
		g.drawString("I was born in South Korea on April 22, 2000.", 200, 650);

	}

	// creates the windmill
	public void createWindmill(Graphics g) {
		// change stroke
		Graphics2D g1 = (Graphics2D) g;
		g1.setStroke(new BasicStroke(3));
		g1.setColor(Color.black);

		// create polygon for base
		Polygon base = new Polygon();
		base.addPoint(300, 600); // left bottom
		base.addPoint(700, 600); // right bottom
		base.addPoint(595, 383); // right semi
		base.addPoint(500, 290); // peak
		base.addPoint(405, 383); // right semi

		createRect(465, 500, 1, "windmill"); // door
		createRect(410, 400, 2, "windmill"); // three windows
		createRect(200, 230, 3, "windmill"); // four blades

		// draw base of windmill
		if (windmill.get("Base").isColorOn()) {
			g1.draw(base);
			g1.setColor(new Color(39, 107, 66));
			g.fillPolygon(base);
		}

		//break the base of the windmill
		g1.drawPolygon(base);

		// draw shapes
		drawShapes(g1, windmill);
		//create lines in the windmill windows
		createLines(g1);

		// create text
		g.setColor(Color.black);
		g.setFont(new Font("SanSerif", 35, 30));
		g.drawString("I lived in the VERY Dutch town of Pella, Iowa for 13 years.", 140, 650);
	}

	// creates the lines on the windmill windows and color if the grouping's color is on
	public void createLines(Graphics g) {
		if (windmill.get("Windows").isColorOn()) {
			g.setColor(new Color(39, 107, 66));
		} else {
			g.setColor(Color.black);
		}

		g.drawLine(540, 425, 590, 425);
		g.drawLine(565, 400, 565, 450);
		g.drawLine(410, 425, 460, 425);
		g.drawLine(435, 400, 435, 450);
		g.drawLine(475, 425, 525, 425);
		g.drawLine(500, 400, 500, 450);

		g.setColor(Color.black);
	}

	// creates HOPE Letters
	public void createHope(Graphics g) {
		for (String group : hope.keySet()) {
			if (hope.get(group).isColorOn()) {
				if (group == "H" || group == "P") {
					g.setColor(new Color(250, 129, 22));
				} else {
					g.setColor(new Color(21, 62, 97));
				}
				drawLetter(g, group);
			} else {
				g.setColor(Color.black);
				drawLetter(g, group);
			}
		}
		
		//create text
		g.setFont(new Font("SanSerif", 35, 30));
		g.drawString("Now, I am a sophomore at Hope College.", 200, 650);
	}

	// draws the individual letters of HOPE
	public void drawLetter(Graphics g, String group) {
		g.setFont(new Font("SanSerif", Font.BOLD, 200));
		switch (group) {
		case "H":
			g.drawString("H ", 150, 400);
			break;
		case "O":
			g.drawString("O", 320, 400);
			break;
		case "P":
			g.drawString("P", 500, 400);
			break;
		case "E":
			g.drawString("E", 650, 400);
			break;
		}
	}

	// return current page hashmap
	public HashMap<String, ColorShape> findPage() {
		HashMap<String, ColorShape> page = new HashMap<>();
		switch (currentPage) {
		case 0:
			page = welcome;
		case 1:
			page = flag;
			break;
		case 2:
			page = windmill;
			break;
		case 3:
			page = hope;
			break;
		}
		return page;
	}

	// returns an Arraylist of groups close to coordinates
	public ArrayList<String> findGroups(int dx, int dy) {
		HashMap<String, ColorShape> page = findPage();
		ArrayList<String> groups = new ArrayList<>();

		for (String group : page.keySet()) {
			int x = page.get(group).getCoordinates("x");
			int y = page.get(group).getCoordinates("y");
			int xrange = x + page.get(group).getWidth();
			int yrange = y + page.get(group).getHeight();

			if (dx > x && dx < xrange && dy > y && dy < yrange && !page.get(group).isColorOn()) {
				groups.add(group); // changeColor(true);
			}
		}
		return groups;
	}

	// turns on the ability of a grouping to be colored in
	public void turnOnColor(int dx, int dy) {
		HashMap<String, ColorShape> page = findPage();
		ArrayList<String> groups = findGroups(dx, dy);
		// if two groupings overlap
		if (groups.size() > 0) {
			if (groups.contains("Windows") && (groups.contains("Base") || groups.contains("Blade1"))) {
				page.get("Windows").changeColor(true);
			} else if (groups.contains("Blade1") && groups.contains("Blade2") && !groups.contains("Base")) {
				page.get("Blade1").changeColor(true);
				page.get("Blade2").changeColor(true);
			} else {
				page.get(groups.get(0)).changeColor(true);
			}
		}
	}

	// allows the page to go to next or go back
	//also resets the userlines arraylist so they disappear when the 'page' turns.
	public void nextOrBack() {
		if (nextBack.isNext() && currentPage < numPages) {
			userLines = new ArrayList<>();
			currentPage++;
			nextBack.turnFalse();
		} else if (nextBack.isBack() && currentPage > 0) {
			userLines = new ArrayList<>();
			currentPage--;
			nextBack.turnFalse();
		}
	}

	/**
	 * This method is called whenever the applet needs to be drawn. This is the most
	 * important method of this class, since without it, we don't see anything.
	 * 
	 * This is the method where you will most likely do all of your coding.
	 */
	public void paintComponent(Graphics g) {
		// Always place this as the first line in paintComponent.
		super.paintComponent(g);

		switch (currentPage) {
		case 0:
			createWelcome(g);
			break;
		case 1:
			createFlag(g);
			break;
		case 2:
			createWindmill(g);
			break;
		case 3:
			createHope(g);
			break;
		}

		// draw userLine
		drawUserLines(g);

		// Draw a string which tells how many mouse clicks
		g.setColor(Color.black);
		g.setFont(new Font("Times Roman", Font.BOLD, 24));
		g.drawString("Kaley Wilson", 5, 700);

	}

// ------------------------------------------------------------------------------------------

// ---------------------------------------------------------------
// A class to handle the mouse events for the applet.
// This is one of several ways of handling mouse events.
// If you do not want/need to handle mouse events, delete the following code.
//
	private class MouseHandler extends MouseAdapter implements MouseMotionListener {
		Point prev = new Point();

		public void mouseClicked(MouseEvent e) {
			// tests if the mouse clicked near an item that can be colored
			turnOnColor(e.getX(), e.getY());

			// goes to next or back a page if the mouse clicked the next or back button
			nextOrBack();

			// After making any changes that will affect the way the screen is drawn,
			// you have to call repaint.
			repaint();
		}

		public void mouseEntered(MouseEvent e) {

		}

		public void mouseExited(MouseEvent e) {
		}

		//if mouse is pressed, it stores the point created in the 'prev' variable
		//also a userLine is created with the selected color and stroke on the colopanel
		public void mousePressed(MouseEvent e) {
			prev = new Point(e.getX(), e.getY());
			userLine = new UserLine(colorpanel.getColor(), colorpanel.getStroke());
		}

		//if mouse if release, the current userline is added to the arraylist of userLines
		public void mouseReleased(MouseEvent e) {
			userLines.add(userLine);
			repaint();
		}

		public void mouseMoved(MouseEvent e) {
		}

		//if the mouse is dragged, the userline adds a bunch of lines together using the 'prev' x and y value
		//and the 'e' x and y values
		//then the 'e' x and y values are stored in the 'prev' variable for the next line to be created
		public void mouseDragged(MouseEvent e) {
			userLine.addLine(new Line2D.Double(prev.getX(), prev.getY(), e.getX(), e.getY()));
			prev = new Point(e.getX(), e.getY());
			repaint();
		}
	}
// End of MouseHandler class

} // Keep this line--it is the end of the PicturePanel class